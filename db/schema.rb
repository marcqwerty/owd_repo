# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190211071044) do

  create_table "brand_contents", force: :cascade do |t|
    t.integer  "brand_id",   limit: 4
    t.string   "banner",     limit: 100
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "brand_models", force: :cascade do |t|
    t.integer  "brand_id",         limit: 4
    t.string   "brand_model_name", limit: 100
    t.string   "status",           limit: 2
    t.text     "description",      limit: 65535
    t.text     "video_url",        limit: 65535
    t.string   "model_type",       limit: 200
    t.string   "season",           limit: 200
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "brands", force: :cascade do |t|
    t.string   "brand_name",   limit: 100
    t.string   "abbr",         limit: 20
    t.string   "logo",         limit: 220
    t.string   "status",       limit: 2
    t.string   "product_type", limit: 20
    t.text     "description",  limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "cart_checkouts", force: :cascade do |t|
    t.integer  "cart_id",        limit: 4
    t.string   "fname",          limit: 100
    t.string   "mname",          limit: 100
    t.string   "lname",          limit: 100
    t.text     "address",        limit: 65535
    t.string   "state",          limit: 100
    t.string   "city",           limit: 100
    t.string   "zipcode",        limit: 10
    t.string   "email",          limit: 100
    t.string   "phone_main",     limit: 20
    t.string   "phone_alt",      limit: 20
    t.text     "notification",   limit: 65535
    t.string   "status",         limit: 20
    t.string   "transaction_id", limit: 100
    t.datetime "purchased_at"
    t.decimal  "grand_total",                  precision: 10
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "cart_checkouts", ["cart_id"], name: "index_cart_checkouts_on_cart_id", using: :btree

  create_table "carts", force: :cascade do |t|
    t.integer  "quantity",     limit: 4
    t.decimal  "price",                    precision: 10
    t.integer  "item_id",      limit: 4
    t.integer  "user_id",      limit: 4
    t.decimal  "discount",                 precision: 10
    t.string   "brand",        limit: 120
    t.string   "model",        limit: 120
    t.string   "tag_location", limit: 20
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.integer  "parent_category_id", limit: 4
    t.string   "meta_keyword",       limit: 255
    t.string   "meta_description",   limit: 255
    t.string   "page_title",         limit: 255
    t.string   "page_layout",        limit: 255
    t.boolean  "status"
    t.text     "description",        limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "cms_categories", force: :cascade do |t|
    t.string   "name",       limit: 100
    t.integer  "parent_id",  limit: 4
    t.string   "status",     limit: 2
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "customer", force: :cascade do |t|
    t.string "fullname", limit: 100, default: ""
    t.string "phone",    limit: 100
    t.string "Address",  limit: 100
    t.string "city",     limit: 100
    t.string "state",    limit: 100
    t.string "zip",      limit: 100
    t.string "email",    limit: 100
  end

  create_table "customer_transaction", force: :cascade do |t|
    t.string   "po_number",                         limit: 100
    t.integer  "store_id",                          limit: 4
    t.string   "store_location",                    limit: 100
    t.integer  "salesperson_id",                    limit: 4
    t.string   "salesperson_name",                  limit: 100
    t.string   "customersname",                     limit: 100
    t.integer  "customer_id",                       limit: 4
    t.string   "customersphone",                    limit: 100
    t.string   "partnumber",                        limit: 100
    t.string   "manufacturecode",                   limit: 100
    t.string   "brand",                             limit: 100
    t.string   "tire",                              limit: 100
    t.string   "appointmentdate",                   limit: 100
    t.string   "appointmenttime",                   limit: 100
    t.integer  "quantity",                          limit: 4
    t.string   "servicetype",                       limit: 100
    t.string   "servicebay",                        limit: 100
    t.integer  "duration",                          limit: 4
    t.string   "sale_rate",                         limit: 100
    t.string   "sale_amount",                       limit: 100
    t.string   "tdf_rate",                          limit: 100
    t.string   "tdf_amount",                        limit: 100
    t.string   "valve_rate",                        limit: 100
    t.string   "valve_amount",                      limit: 100
    t.string   "totaltacxablesale",                 limit: 100
    t.string   "salestax",                          limit: 100
    t.string   "mandb_rate",                        limit: 100
    t.string   "mandb_amount",                      limit: 100
    t.string   "ctrf_rate",                         limit: 100
    t.string   "ctrf_amount",                       limit: 100
    t.string   "total__appointment_sales",          limit: 100
    t.string   "grandtotal_appointment_total",      limit: 100
    t.string   "total__appointment_1",              limit: 100
    t.string   "total__appointment_2",              limit: 100
    t.string   "total__appointment_3",              limit: 100
    t.string   "total__appointment_4",              limit: 100
    t.string   "total__appointment_5",              limit: 100
    t.string   "total__appointment_6",              limit: 100
    t.string   "service_total",                     limit: 100
    t.string   "service_price_total",               limit: 100
    t.string   "service_service_description_total", limit: 100
    t.string   "status",                            limit: 100
    t.string   "reason",                            limit: 100
    t.text     "agents_comments",                   limit: 65535
    t.text     "instruction",                       limit: 65535
    t.datetime "date_created"
  end

  create_table "inventories", force: :cascade do |t|
    t.integer "company_id",               limit: 4
    t.string  "vendor_number",            limit: 100
    t.integer "account_code",             limit: 4
    t.string  "account_code_description", limit: 100
    t.integer "class",                    limit: 4
    t.string  "sales_class_description",  limit: 100
    t.string  "item_number",              limit: 100
    t.string  "tire_catalogsize",         limit: 100
    t.integer "tire_searchsize",          limit: 4
    t.text    "description",              limit: 65535
    t.integer "qty_on_hand",              limit: 4
    t.integer "qty_committed",            limit: 4
    t.integer "qty_available",            limit: 4
    t.integer "recommend_qty",            limit: 4
    t.integer "fet",                      limit: 4
    t.float   "average_cost",             limit: 53
    t.float   "ext_average_cost",         limit: 53
    t.float   "last_cost",                limit: 53
    t.float   "ext_last_cost",            limit: 53
    t.integer "inventoryable",            limit: 4
    t.string  "tire_attrib2",             limit: 100
    t.string  "tire_attrib3",             limit: 100
    t.string  "tire_attrib4",             limit: 100
    t.string  "tire_attrib5",             limit: 100
  end

  create_table "mount_wheels", force: :cascade do |t|
    t.string   "CARTIREID",        limit: 20
    t.integer  "COUNTRYID",        limit: 4
    t.integer  "RIM_DIAMETER_NUM", limit: 4
    t.string   "RIMWIDTH",         limit: 20
    t.decimal  "RIMWIDTH_MIN_NUM",            precision: 10
    t.decimal  "RIMWIDTH_MAX_NUM",            precision: 10
    t.string   "CONTOUR",          limit: 20
    t.string   "WBC",              limit: 20
    t.integer  "BOLTS",            limit: 4
    t.string   "BCD_DISPLAY",      limit: 20
    t.string   "BCD_NUM",          limit: 20
    t.string   "CBD_DISPLAY",      limit: 20
    t.string   "CBD_NUM",          limit: 10
    t.string   "OFFSET_DISPLAY",   limit: 10
    t.string   "OFFSET_SYMBOL",    limit: 10
    t.string   "OFFSET_FRONT_NUM", limit: 20
    t.string   "OFFSET_REAR_NUM",  limit: 20
    t.string   "LUG_NUT_TORQUE",   limit: 20
    t.string   "SIZERIM",          limit: 20
    t.string   "OEHEX",            limit: 20
    t.string   "THREAD",           limit: 20
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "phone_quotes_reasons", force: :cascade do |t|
    t.text "reasons", limit: 65535
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "brand_id",         limit: 4
    t.integer  "brand_model_id",   limit: 4
    t.string   "title",            limit: 100
    t.text     "content",          limit: 65535
    t.string   "brand_model_name", limit: 100
    t.string   "first_name",       limit: 100
    t.string   "last_name",        limit: 100
    t.string   "email",            limit: 100
    t.integer  "stars",            limit: 4
    t.string   "status",           limit: 2
    t.integer  "dry",              limit: 4
    t.integer  "wet",              limit: 4
    t.integer  "snow",             limit: 4
    t.integer  "comfort",          limit: 4
    t.integer  "noise",            limit: 4
    t.integer  "treadwear",        limit: 4
    t.integer  "ride",             limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "service", force: :cascade do |t|
    t.string "service_type",        limit: 100
    t.string "productcode",         limit: 100
    t.string "service_description", limit: 100
    t.string "price",               limit: 100
    t.string "tax",                 limit: 100
    t.string "per_hour",            limit: 100
  end

  create_table "store_inventories", force: :cascade do |t|
    t.string  "brand",       limit: 100
    t.string  "mspn",        limit: 100
    t.string  "size",        limit: 100
    t.string  "description", limit: 100
    t.string  "quantity",    limit: 100
    t.integer "company",     limit: 4
    t.string  "price",       limit: 100
    t.float   "cost",        limit: 53,  default: 0.0
  end

  create_table "store_tax", force: :cascade do |t|
    t.string "COMPANY_NAME",   limit: 100
    t.string "NAME",           limit: 100
    t.string "SALES_TAX_RATE", limit: 100
  end

  create_table "stores", force: :cascade do |t|
    t.integer "store_number",   limit: 4
    t.string  "location",       limit: 100
    t.string  "opening_hours",  limit: 100
    t.string  "phonenumber",    limit: 100
    t.integer "area_code",      limit: 4
    t.string  "faxnumber",      limit: 100
    t.string  "Address",        limit: 100
    t.string  "city",           limit: 100
    t.string  "state",          limit: 100
    t.integer "zip",            limit: 4
    t.string  "email",          limit: 100
    t.integer "region",         limit: 4
    t.integer "district",       limit: 4
    t.string  "coordinates",    limit: 100
    t.string  "sales_tax_rate", limit: 100
    t.decimal "latitude",                   precision: 10, scale: 6
    t.decimal "longitude",                  precision: 10, scale: 6
  end

  add_index "stores", ["latitude", "longitude"], name: "index_stores_on_latitude_and_longitude", using: :btree

  create_table "tire_inventories", force: :cascade do |t|
    t.string   "Brand",                    limit: 200
    t.string   "MSPN",                     limit: 200
    t.string   "Source",                   limit: 200
    t.string   "Valid",                    limit: 200
    t.string   "Oldmspn",                  limit: 200
    t.string   "AltMSPN",                  limit: 200
    t.string   "Erpid",                    limit: 200
    t.string   "ExternalID",               limit: 200
    t.string   "Type",                     limit: 200
    t.string   "Brandid",                  limit: 200
    t.string   "UNQID",                    limit: 200
    t.string   "Manufacturer",             limit: 200
    t.string   "Model",                    limit: 200
    t.string   "LTPRated",                 limit: 200
    t.string   "SizeDimensions",           limit: 200
    t.string   "LTCZRated",                limit: 200
    t.string   "FullSize",                 limit: 200
    t.text     "Description",              limit: 65535
    t.string   "Lengthval",                limit: 200
    t.string   "Sectionwidth",             limit: 200
    t.string   "Sectionwidthunitid",       limit: 200
    t.string   "Aspectratio",              limit: 200
    t.string   "Rimdiameter",              limit: 200
    t.string   "Rimdiameterunitid",        limit: 200
    t.string   "Overalldiameter",          limit: 200
    t.string   "Overalldiameterunitid",    limit: 200
    t.string   "Weighttire",               limit: 200
    t.string   "Weighttireunitid",         limit: 200
    t.string   "Lengthpackage",            limit: 200
    t.string   "Lengthunitid",             limit: 200
    t.string   "Widthpackage",             limit: 200
    t.string   "Widthunitid",              limit: 200
    t.string   "Lengthpackage2",           limit: 200
    t.string   "Lengthunitid2",            limit: 200
    t.string   "Widthpackage2",            limit: 200
    t.string   "Widthunitid2",             limit: 200
    t.string   "Heightpackage",            limit: 200
    t.string   "Heightunitid",             limit: 200
    t.string   "Weightpackage2",           limit: 200
    t.string   "Weightunitid2",            limit: 200
    t.string   "ShippingCostExtra",        limit: 200
    t.string   "ShippingCostFinal",        limit: 200
    t.string   "Sidewallstyle",            limit: 200
    t.string   "Loadindex1",               limit: 200
    t.string   "Loadindex2",               limit: 200
    t.string   "Speedrating",              limit: 200
    t.string   "Loadrange",                limit: 200
    t.string   "Ply",                      limit: 200
    t.string   "Treaddepth",               limit: 200
    t.string   "Treaddepthunitid",         limit: 200
    t.string   "Rimwidth",                 limit: 200
    t.string   "Rimwidthunitid",           limit: 200
    t.string   "Maxrimwidth",              limit: 200
    t.string   "Maxrimwidthunitid",        limit: 200
    t.string   "Minrimwidth",              limit: 200
    t.string   "Minrimwidthunitid",        limit: 200
    t.string   "UTQG",                     limit: 200
    t.string   "Treadwear",                limit: 200
    t.string   "Traction",                 limit: 200
    t.string   "Temperature",              limit: 200
    t.string   "Warrantytype",             limit: 200
    t.string   "Warrantyinmiles",          limit: 200
    t.string   "Maxpsi",                   limit: 200
    t.string   "Maxloadlb",                limit: 200
    t.text     "Oefitments",               limit: 65535
    t.text     "Oefitmentsmodel",          limit: 65535
    t.string   "AmazonDropshipASIN",       limit: 200
    t.string   "AmazonRegASIN",            limit: 200
    t.string   "AmazonRegProdID",          limit: 200
    t.string   "eBayItemID",               limit: 200
    t.string   "Walmartitemid",            limit: 200
    t.string   "Goodyearpartid",           limit: 200
    t.string   "Pepboysid",                limit: 200
    t.string   "Bigoid",                   limit: 200
    t.string   "Simpletire",               limit: 200
    t.string   "Americastireid",           limit: 200
    t.text     "Imageurlfull",             limit: 65535
    t.text     "Imageurlquarter",          limit: 65535
    t.text     "Imageurltread",            limit: 65535
    t.string   "Publishstatus",            limit: 200
    t.string   "Season",                   limit: 200
    t.string   "Tiretypeperformance",      limit: 200
    t.string   "Cartype",                  limit: 200
    t.string   "Country",                  limit: 200
    t.string   "Qualitytier",              limit: 200
    t.string   "RunFlat",                  limit: 200
    t.string   "Rimprotect",               limit: 200
    t.string   "Noisereduction",           limit: 200
    t.string   "Sealinside",               limit: 200
    t.string   "Discontinued",             limit: 200
    t.string   "MappriceYN",               limit: 200
    t.string   "MappriceDollar",           limit: 200
    t.string   "Mappricedate",             limit: 200
    t.string   "Atdpartid",                limit: 200
    t.string   "Ntwpartid",                limit: 200
    t.string   "Tirecopartid",             limit: 200
    t.string   "Turbopartid",              limit: 200
    t.string   "Twiftppartid",             limit: 200
    t.string   "Ramonapartid",             limit: 200
    t.string   "Twwpartid",                limit: 200
    t.string   "Tcipartid",                limit: 200
    t.string   "Tireuniverseid",           limit: 200
    t.string   "Wheelprosid",              limit: 200
    t.string   "Twieastid",                limit: 200
    t.string   "Itdid",                    limit: 200
    t.string   "Wtdid",                    limit: 200
    t.string   "Advancetireid",            limit: 200
    t.string   "Atdinvoiceprice",          limit: 200
    t.string   "NtwinvoicePrice",          limit: 200
    t.string   "Tirecoinvoiceprice",       limit: 200
    t.string   "Turboinvoiceprice",        limit: 200
    t.string   "Twiftpinvoiceprice",       limit: 200
    t.string   "Ramonainvoiceprice",       limit: 200
    t.string   "Twwinvoicepice",           limit: 200
    t.string   "Tciinvoiceprice",          limit: 200
    t.string   "Tireuniverseinvoiceprice", limit: 200
    t.string   "Wheelprosinvoiceprice",    limit: 200
    t.string   "Twieastinvoiceprice",      limit: 200
    t.string   "Itdinvoiceprice",          limit: 200
    t.string   "Wtdinvoiceprice",          limit: 200
    t.string   "Advancetireinvoiceprice",  limit: 200
    t.string   "Mastinvoiceprice",         limit: 200
    t.string   "Toyoinvoiceprice",         limit: 200
    t.string   "Contiinvoiceprice",        limit: 200
    t.string   "Kumhoinvoiceprice",        limit: 200
    t.string   "Goodyearinvoiceprice",     limit: 200
    t.string   "Asin",                     limit: 200
    t.string   "Asin2",                    limit: 200
    t.string   "set_id",                   limit: 200
    t.text     "vct_description",          limit: 65535
    t.string   "info",                     limit: 200
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.string   "recommended",              limit: 10,                             default: "no"
    t.string   "bestseller",               limit: 10,                             default: "no"
    t.string   "promotions",               limit: 10,                             default: "no"
    t.string   "free_shipping",            limit: 10,                             default: "no"
    t.decimal  "price",                                  precision: 10, scale: 6
  end

  create_table "tire_plus_sizes", force: :cascade do |t|
    t.string   "CARTIREID",         limit: 50
    t.string   "SIZEID",            limit: 50
    t.string   "COUNTRYID",         limit: 50
    t.string   "SIZETYPE",          limit: 50
    t.string   "SIZEDESC",          limit: 50
    t.string   "LOADDESC",          limit: 50
    t.string   "PLY",               limit: 120
    t.string   "LOAD_INDEX_SINGLE", limit: 120
    t.string   "LOAD_INDEX_DUAL",   limit: 120
    t.string   "SPEEDRATE",         limit: 120
    t.string   "EMBEDDED_SPEED",    limit: 120
    t.decimal  "MINRIM",                        precision: 10
    t.decimal  "MAXRIM",                        precision: 10
    t.decimal  "MEASRIM",                       precision: 10
    t.string   "PREFIX",            limit: 120
    t.string   "SIZE_PART_1",       limit: 50
    t.string   "SIZE_PART_2",       limit: 50
    t.string   "SIZE_PART_3",       limit: 50
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "tire_rebates", force: :cascade do |t|
    t.string   "manufacturer",   limit: 200
    t.string   "brand_model",    limit: 200
    t.integer  "brand_model_id", limit: 4
    t.string   "partnumber",     limit: 200
    t.integer  "quantity",       limit: 4
    t.string   "rebate",         limit: 200
    t.string   "overprice",      limit: 200
    t.datetime "from_date"
    t.datetime "to_date"
    t.string   "storenumber",    limit: 200
    t.text     "rebatelink",     limit: 65535
    t.string   "promotion",      limit: 200
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "tire_sizes", force: :cascade do |t|
    t.string   "CARTIREID",             limit: 50
    t.string   "COUNTRYID",             limit: 50
    t.string   "SIZEID",                limit: 50
    t.string   "CYEAR",                 limit: 50
    t.string   "MAKE1",                 limit: 120
    t.string   "MAKE2",                 limit: 120
    t.string   "MODEL",                 limit: 120
    t.string   "OPTION1",               limit: 120
    t.string   "OPTION2",               limit: 120
    t.string   "STDOROPT",              limit: 10
    t.string   "FRB",                   limit: 12
    t.string   "TIRESIZE",              limit: 30
    t.string   "LOAD_INDEX_SINGLE",     limit: 30
    t.string   "LOAD_INDEX_DUAL",       limit: 30
    t.string   "LOAD_INDEX_S_STANDARD", limit: 30
    t.string   "LOAD_INDEX_D_STANDARD", limit: 30
    t.string   "SPEEDRATE",             limit: 30
    t.string   "EMBEDDED_SPEED",        limit: 30
    t.string   "PLY",                   limit: 30
    t.text     "NOTES",                 limit: 65535
    t.string   "WEIGHT",                limit: 100
    t.string   "WEIGHT2",               limit: 100
    t.decimal  "BASE",                                precision: 10, scale: 6
    t.decimal  "BASE2",                               precision: 10, scale: 6
    t.string   "SIZEDESC",              limit: 100
    t.string   "RAWSIZE",               limit: 100
    t.string   "PREFIX",                limit: 12
    t.string   "LOADDESC",              limit: 12
    t.string   "SIZE_PART_1",           limit: 50
    t.string   "SIZE_PART_2",           limit: 50
    t.string   "SIZE_PART_3",           limit: 50
    t.string   "RUNFLAT",               limit: 12
    t.string   "TPMS",                  limit: 12
    t.string   "LRF",                   limit: 12
    t.string   "SUFFIX",                limit: 12
    t.string   "VEHTYPE",               limit: 12
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
  end

  create_table "tires", force: :cascade do |t|
    t.string  "brand",               limit: 100
    t.string  "quality",             limit: 100
    t.string  "mspn",                limit: 100
    t.string  "manufacturer",        limit: 100
    t.string  "model",               limit: 100
    t.string  "fullSize",            limit: 100
    t.text    "description",         limit: 65535
    t.integer "sectionwidth",        limit: 4
    t.string  "sectionwidthunitid",  limit: 100
    t.integer "aspectratio",         limit: 4
    t.integer "rimdiameter",         limit: 4
    t.string  "sidewallstyle",       limit: 100
    t.integer "loadindex1",          limit: 4
    t.string  "Speedrating",         limit: 100
    t.string  "Loadrange",           limit: 100
    t.string  "Ply",                 limit: 100
    t.string  "utqg",                limit: 100
    t.string  "warrantyinmiles",     limit: 100
    t.text    "imageurlquarter",     limit: 65535
    t.string  "season",              limit: 100
    t.string  "tiretypeperformance", limit: 100
    t.string  "run_flat",            limit: 100
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "", null: false
    t.string   "encrypted_password",     limit: 255,   default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "first_name",             limit: 50
    t.string   "last_name",              limit: 50
    t.string   "username",               limit: 50
    t.text     "address",                limit: 65535
    t.text     "address2",               limit: 65535
    t.string   "country",                limit: 100
    t.string   "state",                  limit: 100
    t.string   "zip",                    limit: 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vehicles", force: :cascade do |t|
    t.integer "countryid",             limit: 4
    t.integer "cyear",                 limit: 4
    t.string  "make1",                 limit: 100
    t.string  "make2",                 limit: 100
    t.string  "model",                 limit: 100
    t.string  "option1",               limit: 100
    t.string  "option2",               limit: 100
    t.integer "stdoropt",              limit: 4
    t.string  "frb",                   limit: 100
    t.string  "tiresize",              limit: 100
    t.integer "loas_index_single",     limit: 4
    t.integer "load_index_dual",       limit: 4
    t.integer "load_index_s_standard", limit: 4
    t.integer "load_index_d_standard", limit: 4
    t.string  "speedrate",             limit: 100
    t.string  "embedded_speed",        limit: 100
    t.string  "ply",                   limit: 100,   default: ""
    t.text    "notes",                 limit: 65535
    t.string  "weight",                limit: 100
    t.string  "weight2",               limit: 100
    t.string  "base",                  limit: 100
    t.string  "base2",                 limit: 100
    t.string  "sizedesc",              limit: 100
    t.string  "rawsize",               limit: 100
    t.string  "prefix",                limit: 100
    t.string  "loaddesc",              limit: 100
    t.string  "size_part_1",           limit: 100
    t.string  "size_part_2",           limit: 100,   default: ""
    t.string  "size_part_3",           limit: 100
    t.string  "runflat",               limit: 100
    t.string  "tpms",                  limit: 100
    t.string  "lrf",                   limit: 100
    t.string  "suffix",                limit: 100
    t.string  "vehtype",               limit: 100
  end

  create_table "wheel_inventories", force: :cascade do |t|
    t.string   "Brand",                           limit: 255
    t.string   "SubBrand",                        limit: 255
    t.string   "CompleteBrandName",               limit: 255
    t.string   "ManufacturerPartNum",             limit: 255
    t.string   "DealerPartNum",                   limit: 255
    t.string   "ExternalUID",                     limit: 255
    t.string   "ExternalUIDType",                 limit: 255
    t.string   "Status",                          limit: 255
    t.string   "ModelNum",                        limit: 255
    t.string   "ModelName",                       limit: 255
    t.string   "FullModelDescription",            limit: 255
    t.string   "Vendor",                          limit: 255
    t.string   "VendorChannelDescription",        limit: 255
    t.string   "CompleteWheelDescription",        limit: 255
    t.string   "WheelSize",                       limit: 255
    t.string   "WheelDiameter",                   limit: 255
    t.string   "WheelDiameterUnitID",             limit: 255
    t.string   "WheelWidth",                      limit: 255
    t.string   "WheelWidthUnitID",                limit: 255
    t.string   "LugHoles",                        limit: 255
    t.string   "PCD1Metric",                      limit: 255
    t.string   "BoltPattern1Metric",              limit: 255
    t.string   "PCD2Metric",                      limit: 255
    t.string   "BoltPattern2Metric",              limit: 255
    t.string   "PCD1In",                          limit: 255
    t.string   "BoltPattern1In",                  limit: 255
    t.string   "PCD2In",                          limit: 255
    t.string   "BoltPattern2In",                  limit: 255
    t.string   "LoadRating",                      limit: 255
    t.string   "LoadRatingUnitID",                limit: 255
    t.string   "Backspacing",                     limit: 255
    t.string   "BackspacingUnitID",               limit: 255
    t.string   "Offset",                          limit: 255
    t.string   "OffsetUnitID",                    limit: 255
    t.string   "OffsetRangeID",                   limit: 255
    t.string   "CenterBore",                      limit: 255
    t.string   "CenterBoreUnitID",                limit: 255
    t.string   "LipSize",                         limit: 255
    t.string   "LipSizeUnitID",                   limit: 255
    t.string   "ShippingLength",                  limit: 255
    t.string   "ShippingLengthUnitID",            limit: 255
    t.string   "ShippingWidth",                   limit: 255
    t.string   "ShippingWidthUnitID",             limit: 255
    t.string   "ShippingHeight",                  limit: 255
    t.string   "ShippingHeightUnitID",            limit: 255
    t.string   "ShippingWeight",                  limit: 255
    t.string   "ShippingWeightUnitID",            limit: 255
    t.string   "ManufacturersFinishID",           limit: 255
    t.string   "ManufacturersFinishDescription",  limit: 255
    t.string   "ManufacturersFinishDescription2", limit: 255
    t.string   "CODE",                            limit: 255
    t.string   "ImageURL",                        limit: 255
    t.string   "Warranty",                        limit: 255
    t.string   "LugSeat",                         limit: 255
    t.string   "LugHoleType",                     limit: 255
    t.string   "CenterCapType",                   limit: 255
    t.string   "CenterCapCoverLugs",              limit: 255
    t.string   "Construction",                    limit: 255
    t.string   "ReplacementCenterCap",            limit: 255
    t.string   "Material",                        limit: 255
    t.string   "Inserts",                         limit: 255
    t.string   "VehicleApplication",              limit: 255
    t.string   "CountryOfOrigin",                 limit: 255
    t.string   "QualityTier",                     limit: 255
    t.string   "Disc",                            limit: 255
    t.string   "DiscNote",                        limit: 255
    t.string   "VendorPrice",                     limit: 255
    t.string   "MAPPrice",                        limit: 255
    t.string   "MSRP",                            limit: 255
    t.string   "Blowout",                         limit: 255
    t.string   "Inventory",                       limit: 255
    t.string   "InventoryDate",                   limit: 255
    t.string   "CapCover",                        limit: 255
    t.string   "eBayID",                          limit: 255
    t.string   "eBaySellingPrice",                limit: 255
    t.datetime "created_at",                                                                          null: false
    t.datetime "updated_at",                                                                          null: false
    t.string   "recommended",                     limit: 10,                           default: "no"
    t.string   "bestseller",                      limit: 10,                           default: "no"
    t.string   "promotions",                      limit: 10,                           default: "no"
    t.string   "free_shipping",                   limit: 10,                           default: "no"
    t.decimal  "price",                                       precision: 10, scale: 6
  end

  create_table "wheels", force: :cascade do |t|
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "manufacturer_part_num",     limit: 255
    t.string   "unique_id",                 limit: 255
    t.string   "brand",                     limit: 255
    t.string   "sub_brand",                 limit: 255
    t.string   "complete_brand_name",       limit: 255
    t.string   "dealer_part_num",           limit: 255
    t.string   "upc_external_uid",          limit: 255
    t.string   "external_uid_type",         limit: 255
    t.string   "asin",                      limit: 255
    t.string   "status",                    limit: 255
    t.string   "model_number",              limit: 255
    t.text     "full_model_desc",           limit: 65535
    t.string   "vendor",                    limit: 255
    t.text     "short_desc",                limit: 65535
    t.text     "long_desc",                 limit: 65535
    t.text     "vendor_channel_desc",       limit: 65535
    t.text     "complete_wheel_desc",       limit: 65535
    t.string   "wheel_size",                limit: 255
    t.string   "wheel_diameter",            limit: 255
    t.string   "wheel_diameter_unit_id",    limit: 255
    t.string   "wheel_width",               limit: 255
    t.string   "wheel_width_unit_id",       limit: 255
    t.string   "bolt_pattern1_metric",      limit: 255
    t.string   "lug_holes1",                limit: 255
    t.string   "pcd1_metric",               limit: 255
    t.string   "pcd1_in",                   limit: 255
    t.string   "bolt_pattern2_metric",      limit: 255
    t.string   "lug_holes2",                limit: 255
    t.string   "pcd2_metric",               limit: 255
    t.string   "pcd2_in",                   limit: 255
    t.string   "bolt_pattern3_in",          limit: 255
    t.string   "lug_holes3",                limit: 255
    t.string   "pcd3_metric",               limit: 255
    t.string   "pcd3_in",                   limit: 255
    t.string   "load_rating",               limit: 255
    t.string   "load_rating_unit_id",       limit: 255
    t.string   "backspacing",               limit: 255
    t.string   "backspacing_unit_id",       limit: 255
    t.string   "offset",                    limit: 255
    t.string   "offset_unit_id",            limit: 255
    t.string   "offset_range_id",           limit: 255
    t.string   "center_bore",               limit: 255
    t.string   "center_bore_unit_id",       limit: 255
    t.string   "lip_size",                  limit: 255
    t.string   "lip_size_unit_id",          limit: 255
    t.string   "shipping_length",           limit: 255
    t.string   "shipping_length_unit_id",   limit: 255
    t.string   "shipping_width",            limit: 255
    t.string   "shipping_width_unit_id",    limit: 255
    t.string   "shipping_heigth",           limit: 255
    t.string   "shipping_heigth_unit_id",   limit: 255
    t.string   "shipping_weight",           limit: 255
    t.string   "shipping_weight_unit_id",   limit: 255
    t.string   "actual_weight",             limit: 255
    t.string   "actual_weight_unit_id",     limit: 255
    t.string   "manufacturer_finish_id",    limit: 255
    t.string   "manufacturer_finish_desc",  limit: 255
    t.string   "manufacturer_finish_desc2", limit: 255
    t.string   "standard_finish",           limit: 255
    t.string   "simple_finish",             limit: 255
    t.string   "code",                      limit: 255
    t.string   "image_file_name_1",         limit: 255
    t.string   "image_file_name_2",         limit: 255
    t.string   "image_file_name_3",         limit: 255
    t.string   "image_file_name_4",         limit: 255
    t.string   "image_url_1",               limit: 255
    t.string   "image_url_2",               limit: 255
    t.string   "image_url_3",               limit: 255
    t.string   "image_url_4",               limit: 255
    t.string   "image_front_facing_png",    limit: 255
    t.string   "warranty",                  limit: 255
    t.string   "lug_seat",                  limit: 255
    t.string   "lug_hole_type",             limit: 255
    t.string   "center_cap_type",           limit: 255
    t.string   "center_cap_cover_lugs",     limit: 255
    t.string   "construction",              limit: 255
    t.string   "replacement_center_cap",    limit: 255
    t.string   "materials",                 limit: 255
    t.string   "inserts",                   limit: 255
    t.string   "vehicle_application",       limit: 255
    t.string   "tpms",                      limit: 255
    t.string   "country_of_origin",         limit: 255
    t.string   "quality_tier",              limit: 255
    t.string   "disc",                      limit: 255
    t.string   "disc_note",                 limit: 255
    t.string   "atv_cost",                  limit: 255
    t.string   "map_price",                 limit: 255
    t.string   "msrp",                      limit: 255
    t.string   "blowout",                   limit: 255
    t.string   "inventory",                 limit: 255
    t.string   "inventory_date",            limit: 255
    t.string   "cap_cover",                 limit: 255
    t.string   "ebay_id",                   limit: 255
    t.string   "ebay_selling_price",        limit: 255
    t.string   "modelname",                 limit: 255
  end

  create_table "zipcodes", force: :cascade do |t|
    t.string "zipcode",     limit: 100
    t.string "zipcodetype", limit: 100
    t.string "city",        limit: 100
    t.string "state",       limit: 100
    t.string "state_name",  limit: 100
  end

end
