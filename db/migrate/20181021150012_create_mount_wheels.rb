class CreateMountWheels < ActiveRecord::Migration
  def change
    create_table :mount_wheels do |t|
    	t.string   :CARTIREID, limit: 20
    	t.integer  :COUNTRYID
    	t.integer  :RIM_DIAMETER_NUM 
    	t.string   :RIMWIDTH, limit: 20
    	t.decimal  :RIMWIDTH_MIN_NUM
    	t.decimal  :RIMWIDTH_MAX_NUM
    	t.string   :CONTOUR, limit: 20
    	t.string   :WBC, limit: 20
    	t.integer  :BOLTS
    	t.string   :BCD_DISPLAY, limit: 20
    	t.string   :BCD_NUM, limit: 20
    	t.string   :CBD_DISPLAY, limit: 20
    	t.string   :CBD_NUM, limit: 10
    	t.string   :OFFSET_DISPLAY, limit: 10
    	t.string   :OFFSET_SYMBOL, limit: 10
    	t.string   :OFFSET_FRONT_NUM, limit: 20
    	t.string   :OFFSET_REAR_NUM, limit: 20
    	t.string   :LUG_NUT_TORQUE, limit: 20
    	t.string   :SIZERIM, limit: 20
    	t.string   :OEHEX, limit: 20
    	t.string   :THREAD, limit: 20
      t.timestamps null: false
    end
  end
end
