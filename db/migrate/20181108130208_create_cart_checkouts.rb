class CreateCartCheckouts < ActiveRecord::Migration
  def change
    create_table :cart_checkouts do |t|
    	t.references :cart, index: true
    	t.string     :fname, limit: 100
    	t.string     :mname, limit: 100
    	t.string     :lname, limit: 100
    	t.text       :address, limit: 500
    	t.string     :state, limit: 100
    	t.string     :city, limit: 100
    	t.string     :zipcode, limit: 10
    	t.string     :email, limit: 100
    	t.string     :phone_main, limit: 20
    	t.string     :phone_alt, limit: 20
    	t.text       :notification, limit: 1000
    	t.string     :status, limit: 20
    	t.string     :transaction_id, limit: 100
    	t.datetime   :purchased_at
    	t.decimal    :grand_total
    	t.timestamps null: false
    end
  end
end