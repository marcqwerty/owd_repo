class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
    	t.string          :name
    	t.integer         :parent_category_id
    	t.string          :meta_keyword
    	t.string				  :meta_description
    	t.string          :page_title
    	t.string          :page_layout
    	t.boolean            :status
    	t.text            :description         
      t.timestamps null: false
    end
  end
end
