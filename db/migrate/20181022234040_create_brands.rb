class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
    	t.string   :brand_name, limit: 100
    	t.string   :abbr, limit: 20
    	t.string   :logo, limit: 220
    	t.string   :status, limit: 2
    	t.string   :product_type, limit: 20
      t.text     :description, limit: 5000
      t.timestamps null: false
    end
  end
end