class TireSizesAndPlusSize < ActiveRecord::Migration
  def change
  	create_table :tire_sizes do |t|
    	t.string    :CARTIREID, limit: 50
    	t.string    :COUNTRYID, limit: 50
    	t.string    :SIZEID, limit: 50
    	t.string     :CYEAR, limit: 50
    	t.string     :MAKE1, limit: 120
    	t.string     :MAKE2, limit: 120
    	t.string     :MODEL, limit: 120
    	t.string     :OPTION1, limit: 120
    	t.string     :OPTION2, limit: 120
    	t.string     :STDOROPT, limit: 10
    	t.string     :FRB, limit: 12
    	t.string     :TIRESIZE, limit: 30
    	t.string     :LOAD_INDEX_SINGLE, limit: 30
    	t.string     :LOAD_INDEX_DUAL, limit: 30
    	t.string     :LOAD_INDEX_S_STANDARD, limit: 30
    	t.string     :LOAD_INDEX_D_STANDARD, limit: 30
    	t.string     :SPEEDRATE, limit: 30
    	t.string     :EMBEDDED_SPEED, limit: 30
    	t.string     :PLY, limit: 30
    	t.text       :NOTES, limit: 5000
    	t.string     :WEIGHT, limit: 100
    	t.string     :WEIGHT2, limit: 100
    	t.decimal    :BASE, precision: 10, scale: 6
    	t.decimal    :BASE2, precision: 10, scale: 6
    	t.string     :SIZEDESC, limit: 100
    	t.string     :RAWSIZE, limit: 100
    	t.string     :PREFIX, limit: 12
    	t.string     :LOADDESC, limit: 12
    	t.string     :SIZE_PART_1, limit: 50
    	t.string     :SIZE_PART_2, limit: 50
    	t.string     :SIZE_PART_3, limit: 50
    	t.string     :RUNFLAT, limit: 12
    	t.string     :TPMS, limit: 12
    	t.string     :LRF, limit: 12
    	t.string     :SUFFIX, limit: 12
    	t.string     :VEHTYPE, limit: 12
      t.timestamps null: false
    end

    create_table :tire_plus_sizes do |t|
      t.string     :CARTIREID, limit: 50
      t.string     :SIZEID, limit: 50
      t.string     :COUNTRYID, limit: 50
      t.string     :SIZETYPE, limit: 50
      t.string     :SIZEDESC, limit: 50
      t.string     :LOADDESC, limit: 50
      t.string     :PLY, limit: 120
      t.string     :LOAD_INDEX_SINGLE, limit: 120
      t.string     :LOAD_INDEX_DUAL, limit: 120
      t.string     :SPEEDRATE, limit: 120
      t.string     :EMBEDDED_SPEED, limit: 120
      t.decimal    :MINRIM
      t.decimal    :MAXRIM
      t.decimal    :MEASRIM
      t.string     :PREFIX, limit: 120
      t.string     :SIZE_PART_1, limit: 50
      t.string     :SIZE_PART_2, limit: 50
      t.string     :SIZE_PART_3, limit: 50
      t.timestamps null: false
    end
  end
end
