class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
    	t.integer  :brand_id
    	t.integer  :brand_model_id
    	t.string   :title, limit: 100
    	t.text     :content, limit: 5000
    	t.string   :brand_model_name, limit: 100
    	t.string   :first_name, limit: 100
      t.string   :last_name, limit: 100
    	t.string   :email, limit: 100
    	t.integer  :stars
    	t.string   :status, limit: 2
      t.integer  :dry
      t.integer  :wet
      t.integer  :snow
      t.integer  :comfort
      t.integer  :noise
      t.integer  :treadwear
      t.integer  :ride
      t.timestamps null: false
    end
  end
end
