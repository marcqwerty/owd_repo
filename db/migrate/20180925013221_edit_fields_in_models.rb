class EditFieldsInModels < ActiveRecord::Migration
  def change
  	remove_column :brand_models, :model_name
  	add_column :brand_models, :name, :string
  end
end
