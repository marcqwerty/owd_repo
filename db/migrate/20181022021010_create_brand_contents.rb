class CreateBrandContents < ActiveRecord::Migration
  def change
    create_table :brand_contents do |t|
    	t.integer  :brand_id
    	t.string   :banner, limit: 100
    	t.text     :content, limit: 5000
    	t.timestamps null: false
    end
  end
end