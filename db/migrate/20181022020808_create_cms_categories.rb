class CreateCmsCategories < ActiveRecord::Migration
  def change
    create_table :cms_categories do |t|
    	t.string   :name, limit: 100
    	t.integer  :parent_id
    	t.string   :status, limit: 2
      t.timestamps null: false
    end
  end
end
