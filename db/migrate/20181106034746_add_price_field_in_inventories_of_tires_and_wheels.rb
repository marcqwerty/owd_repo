class AddPriceFieldInInventoriesOfTiresAndWheels < ActiveRecord::Migration
  def change
  	add_column :tire_inventories, :price, :decimal, precision: 10, scale: 6

  	add_column :wheel_inventories, :price, :decimal, precision: 10, scale: 6
  end
end
