class EditFieldsInReviews < ActiveRecord::Migration
  def change
  	remove_column :reviews, :model_name
  	add_column :reviews, :brand_model_name, :string
  end
end
