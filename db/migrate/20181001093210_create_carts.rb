class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
    	t.integer  :quantity
    	t.decimal  :price
    	t.integer  :item_id
    	t.integer  :user_id
    	t.decimal  :discount
    	t.string   :brand, limit: 120
    	t.string   :model, limit: 120
    	t.string   :tag_location, limit: 20
      t.timestamps null: false
    end
  end
end
