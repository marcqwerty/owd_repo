class CreateBrandModels < ActiveRecord::Migration
  def change
    create_table :brand_models do |t|  
        t.integer  :brand_id
    	t.string   :brand_model_name, limit: 100
    	t.string   :status, limit: 2
    	t.text     :description, limit: 5000
    	t.text     :video_url, limit: 1000
    	t.string   :model_type, limit: 200
    	t.string   :season, limit: 200
    	t.timestamps null: false
    end
  end
end