class CreateTireRebates < ActiveRecord::Migration
  def change
    create_table :tire_rebates do |t|
    	t.string   :manufacturer, limit: 200
    	t.string   :brand_model, limit: 200
    	t.integer  :brand_model_id
    	t.string   :partnumber, limit: 200
    	t.integer  :quantity
    	t.string   :rebate, limit: 200
    	t.string   :overprice, limit: 200
    	t.datetime     :from_date
    	t.datetime     :to_date
    	t.string   :storenumber, limit: 200
    	t.text     :rebatelink, limit: 2000
    	t.string   :promotion, limit: 200   
      t.timestamps null: false
    end
  end
end
