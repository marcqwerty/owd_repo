class AddFieldsInTiresInventoriesAndWheels < ActiveRecord::Migration
  def change
  	add_column :tire_inventories, :recommended, :string, limit: 10, default: 'no'
  	add_column :tire_inventories, :bestseller, :string, limit: 10, default: 'no'
  	add_column :tire_inventories, :promotions, :string, limit: 10, default: 'no'
  	add_column :tire_inventories, :free_shipping, :string, limit: 10, default: 'no'

  	add_column :wheel_inventories, :recommended, :string, limit: 10, default: 'no'
  	add_column :wheel_inventories, :bestseller, :string, limit: 10, default: 'no'
  	add_column :wheel_inventories, :promotions, :string, limit: 10, default: 'no'
  	add_column :wheel_inventories, :free_shipping, :string, limit: 10, default: 'no'
  end
end
