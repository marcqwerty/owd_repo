require 'csv'

task :load_tgp_size_csv => :environment do
  csv_text = File.read('tgpSize_1808_US_excel_2007.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    TireSize.create(
      CARTIREID: row["CARTIREID"],
      COUNTRYID: row["COUNTRYID"],
      SIZEID: row["SIZEID"],
      CYEAR: row["CYEAR"],
      MAKE1: row["MAKE1"],
      MAKE2: row["MAKE2"],
      MODEL: row["MODEL"],
      OPTION1: row["OPTION1"],
      OPTION2: row["OPTION2"],
      STDOROPT: row["STDOROPT"],
      FRB: row["FRB"],
      TIRESIZE: row["TIRESIZE"],
      LOAD_INDEX_SINGLE: row["LOAD_INDEX_SINGLE"],
      LOAD_INDEX_DUAL: row["LOAD_INDEX_DUAL"],
      LOAD_INDEX_S_STANDARD: row["LOAD_INDEX_S_STANDARD"],
      LOAD_INDEX_D_STANDARD: row["LOAD_INDEX_D_STANDARD"],
      SPEEDRATE: row["SPEEDRATE"],
      EMBEDDED_SPEED: row["EMBEDDED_SPEED"],
      PLY: row["PLY"],
      NOTES: row["NOTES"],
      WEIGHT: row["WEIGHT"],
      WEIGHT2: row["WEIGHT2"],
      BASE: row["BASE"],
      BASE2: row["BASE2"],
      SIZEDESC: row["SIZEDESC"],
      RAWSIZE: row["RAWSIZE"],
      PREFIX: row["PREFIX"],
      LOADDESC: row["LOADDESC"],
      SIZE_PART_1: row["SIZE_PART_1"],
      SIZE_PART_2: row["SIZE_PART_2"],
      SIZE_PART_3: row["SIZE_PART_3"],
      RUNFLAT: row["RUNFLAT"],
      TPMS: row["TPMS"],
      LRF: row["LRF"],
      SUFFIX: row["SUFFIX"],
      VEHTYPE: row["VEHTYPE"]
    )
  end
  puts "End of task"
end