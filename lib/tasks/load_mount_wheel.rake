require 'csv'

task :load_mount_wheel => :environment do
  csv_text = File.read('tgpMount_1808_US_excel_2007.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    MountWheel.create(
      CARTIREID: row["CARTIREID"],
      COUNTRYID: row["COUNTRYID"],
      RIM_DIAMETER_NUM: row["RIM_DIAMETER_NUM"], 
      RIMWIDTH: row["RIMWIDTH"],
      RIMWIDTH_MIN_NUM: row["RIMWIDTH_MIN_NUM"],
      RIMWIDTH_MAX_NUM: row["RIMWIDTH_MAX_NUM"],
      CONTOUR: row["CONTOUR"],
      WBC: row["WBC"],
      BOLTS: row["BOLTS"],
      BCD_DISPLAY: row["BCD_DISPLAY"],
      BCD_NUM: row["BCD_NUM"],
      CBD_DISPLAY: row["CBD_DISPLAY"],
      CBD_NUM: row["CBD_NUM"],
      OFFSET_DISPLAY: row["OFFSET_DISPLAY"],
      OFFSET_SYMBOL: row["OFFSET_SYMBOL"],
      OFFSET_FRONT_NUM: row["OFFSET_FRONT_NUM"],
      OFFSET_REAR_NUM: row["OFFSET_REAR_NUM"],
      LUG_NUT_TORQUE: row["LUG_NUT_TORQUE"],
      SIZERIM: row["SIZERIM"],
      OEHEX: row["OEHEX"],
      THREAD: row["THREAD"]
    )
  end
  puts "End of task"
end

