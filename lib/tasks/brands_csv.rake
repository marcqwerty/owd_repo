require 'csv'

task :brands_csv => :environment do
  csv_text = File.read('brand_wheels.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    Brand.create(
      brand_name: row["brand_name"],
      abbr: row["abbr"],
      logo: row["logo"],
      status: row["status"],
      product_type: row["product_type"],
      description: row["description"]
    )
  end
  puts "End of task"
end
