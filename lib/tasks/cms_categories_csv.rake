require 'csv'

task :cms_categories_csv => :environment do
  csv_text = File.read('cms_categories.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    CmsCategory.create(
      id: row["id"],
      name: row["name"],
      parent_id: row["parent"],
      status: row["status"]
    )
  end
  puts "End of task"
end