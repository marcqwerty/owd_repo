require 'csv'

task :load_tgp_plus_size_csv => :environment do
  csv_text = File.read('tgpPlus_1808_US_excel_2007.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    TirePlusSize.create(
      CARTIREID: row["CARTIREID"],
      SIZEID: row["SIZEID"],
      COUNTRYID: row["COUNTRYID"],
      SIZETYPE: row["SIZETYPE"],
      SIZEDESC: row["SIZEDESC"],
      LOADDESC: row["LOADDESC"],
      PLY: row["PLY"],
      LOAD_INDEX_SINGLE: row["LOAD_INDEX_SINGLE"],
      LOAD_INDEX_DUAL: row["LOAD_INDEX_DUAL"],
      SPEEDRATE: row["SPEEDRATE"],
      EMBEDDED_SPEED: row["EMBEDDED_SPEED"],
      MINRIM: row["MINRIM"],
      MAXRIM: row["MAXRIM"],
      MEASRIM: row["MEASRIM"],
      PREFIX: row["PREFIX"],
      SIZE_PART_1: row["SIZE_PART_1"],
      SIZE_PART_2: row["SIZE_PART_2"],
      SIZE_PART_3: row["SIZE_PART_3"]
    )
  end
  puts "End of task"
end
