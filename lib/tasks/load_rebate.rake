require 'csv'

task :load_rebate => :environment do
  csv_text = File.read('rebates.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    TireRebate.create(
      manufacturer: row["Manufacturer"],
      brand_model: row["Model"],
      brand_model_id: row["ModelID"],
      partnumber: row["PartNumber"],
      quantity: row["Quantity"],
      rebate: row["Rebate"],
      overprice: row["OverPrice"],
      from_date: row["FromDate"],
      to_date: row["ToDate"],
      storenumber: row["StoreNumber"],
      rebatelink: row["RebateLink"],
      promotion: row["Promotion"]
    )
  end
  puts "End of task"
end