task :update_price => :environment do
  puts "Initializing..\n"
  @x = TireInventory.all
  @x.each do |data|
    @rand = rand(50.1..95.5)
    data.price = @rand
    data.save!
  end
  puts "End of task"
end