require "#{Rails.root}/app/helpers/application_helper"
include ApplicationHelper

task :load_coordinates => :environment do
  puts "Initializing..\n"
  @stores = Store.all
  @stores.each do |data|
    if !data.coordinates.nil?
      puts "#{data.coordinates}\n"
      @coordinate = data.coordinates.split(",")
      data.longitude = @coordinate[1].to_f
      data.latitude = @coordinate[0].to_f
      puts "#{@coordinate[0]}\n"
      puts "#{@coordinate[1]}\n"
      data.save!
      puts "data save....\n"
    end
  end
  puts "End of task"
end