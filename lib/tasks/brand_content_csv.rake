require 'csv'

task :brand_content_csv => :environment do
  csv_text = File.read('brand_page.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    BrandContent.create(
      brand_id: row["brand_id"],
      banner: row["banner"],
      content: row["content"]
    )
  end
  puts "End of task"
end