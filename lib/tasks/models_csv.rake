require 'csv'

task :models_csv => :environment do
  csv_text = File.read('brand_model.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    BrandModel.create(
      id: row["model_id"],
      brand_id: row["brand_id"],
      brand_model_name: row["model_name"],
      status: row["status"],
      description: row["description"],
      video_url: row["video_url"],
      model_type: row["type"],
      season: row["season"]
    )
  end
  puts "End of task"
end
