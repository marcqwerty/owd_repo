require 'csv'

task :reviews_csv => :environment do
  csv_text = File.read('reviews.csv')
  csv = CSV.parse(csv_text, headers: true)
  puts "Initializing..\n"
  csv.each_with_index do |row,index|
    Review.create(
      brand_id: row["brand_id"],
      brand_model_id: row["model_id"],
      title: row["title"],
      content: row["content"],
      brand_model_name: row["model"],
      first_name: row["First Name"],
      last_name: row["Last name"],
      email: row["email"],
      stars: row["stars"],
      dry: row["Dry"],
      wet: row["Wet"],
      snow: row["Snow"],
      comfort: row["Comfort"],
      noise: row["Noise"],
      treadwear: row["Treadwear"],
      ride: row["Ride"],
      status: row["status"]
    )
  end
  puts "End of task"
end
