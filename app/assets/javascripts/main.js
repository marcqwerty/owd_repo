$(function(){

	$('.cart-ic').click(function(){
		$('.cart-pop').toggle();
	});

});

$(function () {
  $('[data-toggle="popover"]').popover()
})

$( document ).ready(function() {
    $(".show-fee").click(function() {
        if ($(this).text() == "Show Fee Details +") {
            $(this).text("Hide Fee Details -");
            $(".cart-top-2").show();
        } else {
            $(this).text("Show Fee Details +"); 
            $(".cart-top-2").hide();
        }; 
    });

	    // Radio box border
	  $('.method').on('click', function() {
	    $('.method').removeClass('blue-border');
	    $(this).addClass('blue-border');
	  });

	  // Validation
	  /*var $cardInput = $('.input-fields input');

	  $('.next-btn').on('click', function(e) {

	    $cardInput.removeClass('warning');

	    $cardInput.each(function() {
	       var $this = $(this);

	       if (!$this.val()) {
	         $this.addClass('warning');
	       }
	    });

	  });*/
 });


$(document).ready(function() {
  $("label").on("click", function(e) {
    e.preventDefault();
    var $radio = $("#" + $(this).attr("for")),
      name = $radio.attr("name"),
      hasRadio = $radio.attr("type") == "radio";
    if (!hasRadio) return;
    if ($radio.data("is-checked") == true) {
      $radio.prop("checked", false).change();
      $radio.data("is-checked", false);
    } else {
      $radio.data("is-checked", true);
      $radio.prop("checked", true).change();
    }
    $('input[type="radio"][name="' + name + '"]')
      .not("#" + $(this).attr("for"))
      .data("is-checked", false);
  });
});
/* Updated JS this is for menu wheels and tires */
$(".wheelstiresmenu").on('click',function(){
    $('#searchMenuWT').modal('toggle')
});
$(function(){
    $('#custom-search-year #custom-menuSearch').keyup(function(){  
        var current_query = $('#custom-search-year #custom-menuSearch').val();
        if (current_query !== "") {
            $("#custom-search-year .list-group li").hide();
            $("#custom-search-year .list-group li").each(function(){
                var current_keyword = $(this).text();
                if (current_keyword.indexOf(current_query) >=0) {
                    $(this).show();         
                };
            });     
        } else {
            $("#custom-search-year .list-group li").show();
        };
    });
    $('#custom-search-make #custom-menuSearch2').keyup(function(){  
        var current_query = $('#custom-search-make #custom-menuSearch2').val();
        if (current_query !== "") {
            $("#custom-search-make .list-group li").hide();
            $("#custom-search-make .list-group li").each(function(){
                var current_keyword = $(this).text();
                if (current_keyword.indexOf(current_query) >=0) {
                    $(this).show();         
                };
            });     
        } else {
            $("#custom-search-make .list-group li").show();
        };
    });
    $('#custom-search-model #custom-menuSearch3').keyup(function(){  
        var current_query = $('#custom-search-model #custom-menuSearch3').val();
        if (current_query !== "") {
            $("#custom-search-model .list-group li").hide();
            $("#custom-search-model .list-group li").each(function(){
                var current_keyword = $(this).text();
                if (current_keyword.indexOf(current_query) >=0) {
                    $(this).show();         
                };
            });     
        } else {
            $("#custom-search-model .list-group li").show();
        };
    });
    $('#custom-search-option #custom-menuSearch4').keyup(function(){  
        var current_query = $('#custom-search-option #custom-menuSearch4').val();
        if (current_query !== "") {
            $("#custom-search-model .list-group li").hide();
            $("#custom-search-model .list-group li").each(function(){
                var current_keyword = $(this).text();
                if (current_keyword.indexOf(current_query) >=0) {
                    $(this).show();         
                };
            });     
        } else {
            $("#custom-search-option .list-group li").show();
        };
    });
    $("#custom-search-year .list-group li a").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-make').addClass('active');
        var textYear = $( this ).text();
        $('#customSearchVechile>ul>li>a#customSearchYear').text(textYear);
        $('#customSearchVechile>ul>li>a#customSearchYear').addClass('active');
    });
    $("#custom-search-make .list-group li a").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-model').addClass('active');
        var textMake = $( this ).text();
        $('#customSearchVechile>ul>li>a#customSearchMake').text(textMake);
        $('#customSearchVechile>ul>li>a#customSearchYear').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchMake').addClass('active');
    });
    $("#custom-search-model .list-group li a").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-option').addClass('active');
        var textModel = $( this ).text();
        $('#customSearchVechile>ul>li>a#customSearchModel').text(textModel);
        $('#customSearchVechile>ul>li>a#customSearchYear').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchMake').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchModel').addClass('active');
    });
    $("#custom-search-option .list-group li a").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-last').addClass('active');
        var textOption = $( this ).text();
        $('#customSearchVechile>ul>li>a#customSearchOption').text(textOption);
        $('#customSearchVechile>ul>li>a#customSearchYear').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchMake').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchModel').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchOption').addClass('active');
    });
    $("#customSearchYear").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-year').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchYear').text('Year');
        $('#customSearchVechile>ul>li>a#customSearchMake').text('Make');
        $('#customSearchVechile>ul>li>a#customSearchModel').text('Model');
        $('#customSearchVechile>ul>li>a#customSearchOption').text('Option');
        $('#customSearchVechile>ul>li>a').removeClass('active');
    });
    $("#customSearchMake").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-make').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchMake').text('Make');
        $('#customSearchVechile>ul>li>a#customSearchModel').text('Model');
        $('#customSearchVechile>ul>li>a#customSearchOption').text('Option');
        $('#customSearchVechile>ul>li>a').removeClass('active');
        $('#customSearchVechile>ul>li>a#customSearchYear').addClass('active');
    });
    $("#customSearchModel").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-model').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchModel').text('Model');
        $('#customSearchVechile>ul>li>a#customSearchOption').text('Option');
        $('#customSearchVechile>ul>li>a').removeClass('active');
        $('#customSearchVechile>ul>li>a#customSearchYear').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchMake').addClass('active');
    });
    $("#customSearchOption").on('click',function(){
        $('#customSearchVechile>div').removeClass('active');
        $('#custom-search-option').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchOption').text('Option');
        $('#customSearchVechile>ul>li>a').removeClass('active');
        $('#customSearchVechile>ul>li>a#customSearchYear').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchMake').addClass('active');
        $('#customSearchVechile>ul>li>a#customSearchModel').addClass('active');
    });
});