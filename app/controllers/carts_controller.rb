class CartsController < ApplicationController
	before_filter :set_cart, only: [:show, :update, :destroy]

  def index
    @carts = Cart.all
  end

  def new
  	@cart = Cart.new
  end
  
  def edit
  	@cart = Cart.find params[:id]
  end

  def show
    @cart = Cart.find(params[:id])
  end

  def create
  	cart = Cart.new(cart_params)
  	cart_params[:price] = cart_params[:price].to_f
    if cart.save
      if params[:commit] == "Add To Cart"
      	flash[:success] = 'Entry was successfully created.'
      	redirect_to carts_path(zip: params[:zipcode])
      else
        if params[:cart][:tag_location] == 'add_to_cart_wheels'
      		redirect_to list_all_tires_path(make: params[:make], model: params[:model], option: params[:option], search: params[:search], 
      		sp1: params[:sp1], sp2: params[:sp2], sp3: params[:sp3], st: params[:sw], str_num: params[:str_num], year: params[:year],
      		zipcode: params[:zipcode], add_to_cart: true)
      	else
      		redirect_to list_all_wheels_path(make: params[:make], model: params[:model], option: params[:option], search: params[:search], 
      		sp1: params[:sp1], sp2: params[:sp2], sp3: params[:sp3], sw: params[:st], str_num: params[:str_num], year: params[:year],
      		zipcode: params[:zipcode], add_to_cart: true)
      	end
      end
    else
      flash[:error] = 'An error occured while creating new cart.'
      render 'new'
    end
  end


# make=Honda&model=Civic&option=GX&search=true&sp1=6&sp2=6&sp3=15&str_num=18&sw=true&year=2010&zipcode=90210
# make=Honda&model=Civic&option=GX&sp1=195&sp2=65&sp3=15&str_num=18&year=2010&zipcode=90210
  def update
  	cart = Cart.find params[:id]
    if cart.update(cart_params)
      flash[:success] = 'Entry was successfully updated.'
      redirect_to carts_path
    else
      flash[:error] = 'An error occured while creating new cart.'
      render 'new'
    end
  end
  
  def destroy
  	cart = Cart.find params[:id]
    cart.destroy!
    flash[:success] = 'Entry has been deleted.' 
    redirect_to carts_path
  end

  private

    def set_cart
      @cart = Cart.find(params[:id])
    end
      
    def cart_params
      params.require(:cart).permit(:price, :item_id, :user_id, :quantity, 
      														 :discount, :brand, :model, :tag_location)
    end
end