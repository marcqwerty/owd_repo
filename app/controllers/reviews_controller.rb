class ReviewsController < ApplicationController
	before_filter :set_review, only: [:update, :destroy]

  def index
    @reviews = Review.all
    @review = Review.new
  end

  def create
  	review = Review.new(review_params)
    if review.save
      flash[:success] = 'Entry was successfully created.'
    	redirect_to product_page_path(id: params[:id], make: params[:make], model: params[:model], option: params[:option], search: params[:search], 
      		sp1: params[:sp1], sp2: params[:sp2], sp3: params[:sp3], st: params[:st], year: params[:year],
      		zipcode: params[:zipcode])
    else
      flash[:error] = 'An error occured while creating new review.'
      render 'new'
    end
  end

	def update
  	review = Review.find params[:id]
    if review.update(review_params)
      flash[:success] = 'Entry was successfully updated.'
      redirect_to reviews_path
    else
      flash[:error] = 'An error occured while creating new review.'
      render 'new'
    end
  end
  
  def destroy
  	review = Review.find params[:id]
    review.destroy!
    flash[:success] = 'Entry has been deleted.' 
    redirect_to reviews_path
  end

  private

    def set_review
      @review = Review.find(params[:id])
    end
      
    def review_params
      params.require(:review).permit(:brand_id, :brand_model_id, :title,
      						:content, :brand_model_name, :first_name, :last_name,
      						:email, :status, :stars, :dry, :wet, :snow, :comfort,
      						:noise, :treadwear, :ride
      						)
    end
end