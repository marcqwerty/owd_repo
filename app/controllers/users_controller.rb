class UsersController < ApplicationController
	before_filter :set_user, only: [:show, :update, :destroy]

  def index
    @users = User.all #User.paginate(:page => params[:page], :per_page => 20)
    # render json: @items
  end

  def new
    @user = User.new
  end
  
  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)

    if @user.save
      flash[:success] = 'User successfully created.'
      # redirect_to users_path
      redirect_to root_path
    else
      flash[:error] = 'An error occured while creating new user.'
      # render 'new'
      redirect_to root_path
    end
  end

  def update
    if @user.update(item_params)
      flash[:success] = 'User has been modified.'
      redirect_to users_path
    else
      flash[:error] = 'An error occured while creating new user.'
      render 'new'
    end
  end
  
  def destroy
    @user.destroy!
    flash[:success] = 'User has been deleted.' 
    redirect_to users_path
  end

  private

    def set_user
      @user = User.find(params[:id])
    end
      
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :address, :address2, :country, :state, :zip)
    end
end
