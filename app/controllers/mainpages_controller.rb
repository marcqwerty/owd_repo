class MainpagesController < ApplicationController
	def index
		 @makes = Vehicle.select(:make2).distinct.order('make2 ASC')
		 @years = Vehicle.select(:cyear).distinct.order('cyear DESC')
		 @models = Vehicle.select(:model).distinct.order('model ASC')
		 @options = Vehicle.select(:option2).distinct.order('option2 ASC')
		 
		 @Sectionwidth = TireInventory.select(:Sectionwidth).distinct.order('Sectionwidth DESC')
		 @Aspectratio = TireInventory.select(:Aspectratio).distinct.order('Aspectratio ASC')
		 @Rimdiameter = TireInventory.select(:Rimdiameter).distinct.order('Rimdiameter ASC')
		 @WheelDiameter = WheelInventory.select(:WheelDiameter).distinct.order('WheelDiameter ASC')
		 @WheelWidth = WheelInventory.select(:WheelWidth).distinct.order('WheelWidth ASC')
		 if !params[:search].nil?
		 	# @tires = Tire.where("brand LIKE ? AND model LIKE ?", "%#{params[:make]}%", "%#{params[:model]}%").order("section_width ASC")
		 	@vehicles = Vehicle.select("size_part_1, size_part_2, size_part_3").where({cyear: params['year'], make2: params[:make], model: params[:model], option2: params[:option]}).distinct
		 	
		 	# select size_part_1,size_part_2,size_part_3 from vehicle where cyear = '2018' and make2 = 'Honda' and model = 'Accord' and option2 = 'Sport';
		 end
	end

	def search_entry
			
	end

	def update_makes
		@makes = Vehicle.select('make2').where(cyear: params[:cyear]).distinct.order('make2 ASC').map{|b| [b.make2, b.make2]}
	end

	def update_models
		@models = Vehicle.select('model').where(cyear: params[:cyear]).where(make2: params[:make2]).distinct.order('model DESC').map{|b| [b.model, b.model]}
	end

	def update_options
		@options = Vehicle.select('option2').where(cyear: params[:cyear]).where(make2: params[:make2]).where(model: params[:model]).distinct.order('option2 ASC').map{|b| [b.option2, b.option2]}
	end

	def update_years
		@models = Vehicle.where(make2: params[:make], cyear: params[:year])
		respond_to do |format|
      format.js
    end
	end


	def update_aspectratio
		@Aspectratio = TireInventory.select('Aspectratio').where(Sectionwidth: params[:sectionwidth]).distinct.order('Aspectratio ASC').map{|b| [b.Aspectratio, b.Aspectratio]}
	end

	def update_rimdiameter
		@Rimdiameter = TireInventory.select('Rimdiameter').where({Sectionwidth: params[:sectionwidth], Aspectratio: params[:aspectratio]}).distinct.order('Rimdiameter ASC').map{|b| [b.Rimdiameter, b.Rimdiameter]}
	end

	def update_wheelwidth
		@WheelWidth = WheelInventory.select('WheelWidth').where(WheelDiameter: params[:wheeldiameter]).distinct.order('WheelWidth ASC').map{|b| [b.WheelWidth, b.WheelWidth]}
	end

	def search_by_size
		@cart = Cart.new
		if params[:WheelDiameter].empty?
			@choice = "tires"
			@size = TireSize.select("RAWSIZE").where({SIZE_PART_1: params[:Sectionwidth], SIZE_PART_2: params[:Aspectratio], SIZE_PART_3: params[:Rimdiameter]}).distinct
			@data = StoreInventory.select("mspn, price").where(size: @size.first.RAWSIZE).distinct
			@data2 = StoreInventory.select("mspn").where(size: @size.first.RAWSIZE).distinct.pluck(:mspn)
			@tires_catalogue2 = TireInventory.where(MSPN: @data2)
			# @data = TireInventory.where({Sectionwidth: params[:Sectionwidth], Aspectratio: params[:Aspectratio], Rimdiameter: params[:Rimdiameter]}).paginate(:page => params[:page], :per_page => 10)
			# @data2 = TireInventory.where({Sectionwidth: params[:Sectionwidth], Aspectratio: params[:Aspectratio], Rimdiameter: params[:Rimdiameter]}).pluck(:Brand, :MSPN, :id)
		
			@speedratings = @tires_catalogue2.distinct.pluck(:Speedrating)
		  @load_indexes = @tires_catalogue2.distinct.pluck(:Loadindex1)
		  @sidewallstyles = @tires_catalogue2.distinct.pluck(:Sidewallstyle)
		else
			@choice = "wheels"
			@data = WheelInventory.where({WheelDiameter: params[:WheelDiameter], WheelWidth: params[:WheelWidth]}).distinct
			# @data2 = WheelInventory.where({WheelDiameter: params[:WheelDiameter], WheelWidth: params[:WheelWidth]}).distinct.pluck(:mspn)
			# @speedratings = @data.distinct.pluck(:Speedrating)
		 #  @load_indexes = @data.distinct.pluck(:Loadindex1)
		 #  @sidewallstyles = @data.distinct.pluck(:Sidewallstyle)
		end
	end

	def search_tires
		# @vehicles = Vehicle.select("size_part_1, size_part_2, size_part_3").where({cyear: params['year'], make2: params[:make], model: params[:model], option2: params[:option]}).distinct
		@tgp_sizes = TireSize.select("SIZE_PART_1, SIZE_PART_2, SIZE_PART_3, FRB").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		@cartire_ids = TireSize.select("CARTIREID").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct.pluck(:CARTIREID)
		@tgp_plus_sizes = TirePlusSize.select("SIZE_PART_1, SIZE_PART_2, SIZE_PART_3").where(CARTIREID: @cartire_ids).distinct
	end

	def search_wheels
		# @tgp_sizes = TireSize.select("CARTIREID").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		# @tgp_mounts = MountWheel.where(CARTIREID: @tgp_sizes).distinct
		# @tgp_plus_sizes = TirePlusSize.select("CARTIREID").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		# @tgp_plus_mounts = MountWheel.where(CARTIREID: @tgp_sizes).distinct

		@tgp_mounts = TireSize.select("SIZE_PART_1, SIZE_PART_2, SIZE_PART_3").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		@cartire_ids = TireSize.select("CARTIREID").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct.pluck(:CARTIREID)
		@tgp_plus_mounts = TirePlusSize.select("SIZE_PART_1, SIZE_PART_2, SIZE_PART_3").where(CARTIREID: @cartire_ids).distinct

	end

	def search_wheels_tires
		@tgp_sizes = TireSize.select("SIZE_PART_1, SIZE_PART_2, SIZE_PART_3").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		@cartire_ids = TireSize.select("CARTIREID").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct.pluck(:CARTIREID)
		@tgp_plus_sizes = TirePlusSize.select("SIZE_PART_1, SIZE_PART_2, SIZE_PART_3, MINRIM, MAXRIM").where(CARTIREID: @cartire_ids).distinct
	end

	def search_locations
		# @zip = Zipcode.where(zipcode: params[:zipcode])
		# @keyword = "#{@zip.first.city.capitalize}, #{@zip.first.state_name}"
		# this should be the actual search via geokit rails
		# @search =Geokit::Geocoders::GoogleGeocoder.geocode(params[:zipcode])
		# @stores = Store.closest(:origin =>[@search.lat, @search.lng]).limit(10)
		
		@stores = Store.limit(15)

		@count_locations = @stores.count(:all)
	end

	def list_all_tires
		# @tires = Tire.where(sectionwidth: params[:sp1], aspectratio: params[:sp2], rimdiameter: params[:sp3])
		if params[:brands].present?
			@brands = params[:brands]
		else
			@brands = ['ACCELERA', 'ACHILLES', 'ARROYO', 'ATTURO', 'BFGOODRICH', 'BRIDGESTONE']
		end

		
		# old list connecting to inventory
		# @size = TireSize.select("RAWSIZE").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		# @inventory_view = StoreInventory.select("mspn, price").where(size: @size.first.RAWSIZE).distinct
		
		# new listing with price in catalogue
		@size = TireSize.select("RAWSIZE").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		@inventory_view = TireInventory.where(Sectionwidth: params[:sp1], Aspectratio: params[:sp2], Rimdiameter: params[:sp3])
		# add filter
		@inventory_view = @inventory_view.where(recommended: "yes") if params[:r].present?
		@inventory_view = @inventory_view.where(bestseller: "yes") if params[:b].present?
		@inventory_view = @inventory_view.where(promotions: "yes") if params[:p].present?
		@inventory_view = @inventory_view.where(free_shipping: "yes") if params[:f].present?
		@inventory_view = @inventory_view.order("price ASC")
		# @products.status(params[:status]) if params[:status].present?


		# retain
		@inventory_view2 = StoreInventory.select("mspn").where(size: @size.first.RAWSIZE).distinct.pluck(:mspn)
		@tires_catalogue2 = TireInventory.where(MSPN: @inventory_view2)
		# @tires_catalogue = TireInventory.where(Sectionwidth: params[:sp1], Aspectratio: params[:sp2], Rimdiameter: params[:sp3]).where(Brand: @brands).paginate(:page => params[:page], :per_page => 10)
		# @tires_catalogue2 = TireInventory.where(Sectionwidth: params[:sp1], Aspectratio: params[:sp2], Rimdiameter: params[:sp3]).where(Brand: @brands)
		
		@speedratings = @tires_catalogue2.distinct.pluck(:Speedrating)
		@load_indexes = @tires_catalogue2.distinct.pluck(:Loadindex1)
		@sidewallstyles = @tires_catalogue2.distinct.pluck(:Sidewallstyle)

		@cart = Cart.new
	end

	def list_all_wheels
		@cart = Cart.new
		if params[:add_to_cart] == 'true'
			@wheels_list = WheelInventory.where({WheelDiameter: params[:sp3]})
		else
			@tgp_plus_sizes = TirePlusSize.select("MINRIM, MAXRIM, SIZE_PART_3").where(SIZE_PART_1: params[:sp1], SIZE_PART_2: params[:sp2], SIZE_PART_3: params[:sp3]).distinct
			# @wheels_list = WheelInventory.where(WheelDiameter: @tgp_plus_sizes.first.SIZE_PART_3).where("WheelWidth >= ? AND WheelWidth <= ?", @tgp_plus_sizes.first.MINRIM, @tgp_plus_sizes.first.MAXRIM)
			@wheels_list = WheelInventory.where(WheelDiameter: @tgp_plus_sizes.first.SIZE_PART_3).where(WheelWidth: @tgp_plus_sizes.first.MINRIM..@tgp_plus_sizes.first.MAXRIM)
		end
	end
  
	def list_wheels_tires
		@cart = Cart.new
		@size = TireSize.select("RAWSIZE, SIZE_PART_1, SIZE_PART_2, SIZE_PART_3").where({CYEAR: params['year'], MAKE2: params[:make], MODEL: params[:model], OPTION2: params[:option]}).distinct
		@tgp_wheels_search_value = TirePlusSize.select("SIZE_PART_3, MINRIM, MAXRIM").where(SIZE_PART_1: @size.first.SIZE_PART_1, SIZE_PART_2: @size.first.SIZE_PART_2, SIZE_PART_3: @size.first.SIZE_PART_3).distinct
		@inventory_view = StoreInventory.select("mspn, price").where(size: @size.first.RAWSIZE).order("price ASC").distinct
		@inventory_view2 = StoreInventory.select("mspn").where(size: @size.first.RAWSIZE).distinct.pluck(:mspn)
		@tires_catalogue2 = TireInventory.where(MSPN: @inventory_view2)
	end

	def product_page
		@prod_info = TireInventory.find params[:id]
		@review = Review.new
		@cart = Cart.new
	end
  
	def compare_price
	  redirect_to request.referrer
	end

	def comparison
		@ids = params[:compare_ids].split(",")
		@compare_tires = TireInventory.where(id: @ids).limit(4)
	end

	def tires_per_brand_list
		@inventory_view = TireInventory.where(Brand: params[:br], Model: params[:md])
		# add filter
		@inventory_view = @inventory_view.where(recommended: "yes") if params[:r].present?
		@inventory_view = @inventory_view.where(bestseller: "yes") if params[:b].present?
		@inventory_view = @inventory_view.where(promotions: "yes") if params[:p].present?
		@inventory_view = @inventory_view.where(free_shipping: "yes") if params[:f].present?
		@inventory_view = @inventory_view.order("price ASC")
		# @products.status(params[:status]) if params[:status].present?


		# retain
		@inventory_view2 = TireInventory.select("MSPN").where(Brand: params[:br], Model: params[:md]).distinct.pluck(:mspn)
		@tires_catalogue2 = TireInventory.where(MSPN: @inventory_view2)
		# @tires_catalogue = TireInventory.where(Sectionwidth: params[:sp1], Aspectratio: params[:sp2], Rimdiameter: params[:sp3]).where(Brand: @brands).paginate(:page => params[:page], :per_page => 10)
		# @tires_catalogue2 = TireInventory.where(Sectionwidth: params[:sp1], Aspectratio: params[:sp2], Rimdiameter: params[:sp3]).where(Brand: @brands)
		
		@speedratings = @tires_catalogue2.distinct.pluck(:Speedrating)
		@load_indexes = @tires_catalogue2.distinct.pluck(:Loadindex1)
		@sidewallstyles = @tires_catalogue2.distinct.pluck(:Sidewallstyle)

		@cart = Cart.new
	end
end