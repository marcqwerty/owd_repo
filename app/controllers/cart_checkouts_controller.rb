class CartCheckoutsController < ApplicationController
	before_filter :set_cart_checkout, only: [:show]

  def index
  	# @amount = 
    @cart_checkout = CartCheckout.new
  end

  def new
  	@cart_checkout = CartCheckout.new
  end
  
  def edit
  	@cart_checkout = CartCheckout.find params[:id]
  end

  def show
    @cart_checkout = CartCheckout.find(params[:id])
  end

  def create
  	@cart_checkout = CartCheckout.new(cart_checkout_params)
    if @cart_checkout.save
      redirect_to @cart_checkout.paypal_url(cart_checkout_path(@cart_checkout))
    else
    	render :new
    end
  end

  private

    def set_cart_checkout
      @cart_checkout = CartCheckout.find(params[:id])
    end
      
    def cart_checkout_params
      params.require(:cart_checkout).permit(
      		:cart_id, :fname, :mname, :lname, :address, :state, :city, :zipcode, :email, :phone_main,
      		:phone_alt, :notification, :status, :transaction_id, :purchased_at, :grand_total
      	)
    end
end