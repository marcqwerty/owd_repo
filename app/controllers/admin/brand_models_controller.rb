class Admin::BrandModelsController < ApplicationController
	def index
    @brand_models = BrandModel.paginate(:page => params[:page], :per_page => 20)
  end
end
