class Admin::BrandsController < ApplicationController
	before_filter :set_brand, only: [:show, :update, :destroy]

  def index
    @brands = Brand.paginate(:page => params[:page], :per_page => 20)
  end

  def new
  	@brand = Brand.new
  end
  
  def edit
  	@brand = Brand.find params[:id]
  end

  def show
    @brand = Brand.find(params[:id])
  end

  def create
  	brand = Brand.new(brand_params)

    if brand.save
      flash[:success] = 'Entry was successfully created.'
      redirect_to admin_brands_path
    else
      flash[:error] = 'An error occured while creating new brand.'
      render 'new'
    end
  end

  def update
  	brand = Brand.find params[:id]
    if brand.update(brand_params)
      flash[:success] = 'Entry was successfully updated.'
      redirect_to admin_brands_path
    else
      flash[:error] = 'An error occured while creating new brand.'
      render 'new'
    end
  end
  
  def destroy
  	brand = Brand.find params[:id]
    brand.destroy!
    flash[:success] = 'Entry has been deleted.' 
    redirect_to admin_brands_path
  end

  private

    def set_brand
      @brand = Brand.find(params[:id])
    end
      
    def brand_params
      params.require(:brand).permit(:brand_name, :abbr, :logo, :status, :product_type )
    end
end