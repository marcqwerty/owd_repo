class Admin::ReviewsController < ApplicationController
	before_filter :set_review, only: [:show, :update, :destroy]

  def index
    @reviews = Review.all
  end

  def new
  	@review = Review.new
  end
  
  def edit
  	@review = Review.find params[:id]
  end

  def show
    @review = Review.find(params[:id])
  end

  def create
  	review = Review.new(review_params)

    if review.save
      flash[:success] = 'Entry was successfully created.'
      redirect_to reviews_path
    else
      flash[:error] = 'An error occured while creating new review.'
      render 'new'
    end
  end

  def update
  	review = Review.find params[:id]
    if review.update(review_params)
      flash[:success] = 'Entry was successfully updated.'
      redirect_to reviews_path
    else
      flash[:error] = 'An error occured while creating new review.'
      render 'new'
    end
  end
  
  def destroy
  	review = Review.find params[:id]
    review.destroy!
    flash[:success] = 'Entry has been deleted.' 
    redirect_to reviews_path
  end

  private

    def set_review
      @review = Review.find(params[:id])
    end
      
    def review_params
      params.require(:review).permit(:title, :entry_date, :description, :image_file, :ads_position )
    end
end
