class TiresController < ApplicationController
	def tire_brands
		@brand_name = Brand.find_by_brand_name(params[:title].upcase)
		@model_types = BrandModel.where(brand_id: @brand_name.id)
		@model_type_groups = BrandModel.where(brand_id: @brand_name.id).group("model_type Desc")
	end
end
