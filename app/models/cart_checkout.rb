class CartCheckout < ActiveRecord::Base
	belongs_to :cart
  
  serialize :notification_params, Hash
	def paypal_url(return_path)
    values = {
        business: "reynan.albaredo-facilitator@gmail.com",
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}#{return_path}",
        invoice: id,
        amount: self.grand_total,
        item_name: 'Set of Tires',
        item_number: '#136640',
        quantity: self.quantity,
        notify_url: "#{Rails.application.secrets.app_host}/hook"
    }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end

  # serialize :notification_params, Hash
  # def paypal_url(return_path)
  #   values = {
  #       business: "reynan.albaredo-facilitator@gmail.com",
  #       cmd: "_xclick",
  #       upload: 1,
  #       return: "#{Rails.application.secrets.app_host}#{return_path}",
  #       invoice: id,
  #       amount: self.invested_amount,
  #       notify_url: "#{Rails.application.secrets.app_host}/hook"
  #   }
  #   "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  # end
end
