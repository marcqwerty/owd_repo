class Zipcode < ActiveRecord::Base

	def zipcode_full
		self.zipcode + '-' + self.city + ', ' + self.state
	end
	
end
