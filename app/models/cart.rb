class Cart < ActiveRecord::Base
	require 'active_shipping'	

	def price=(price)
    if price.is_a?(String)
      formatted_amount = price.to_f
      super(formatted_amount.round(2))
    end
  end

  def origin
    ActiveShipping::Location.new(country: "US", state: "CA", city: "Los Angeles", postal_code: "90001")
  end

  def destination
    # Location.new(country: country, state: state, city: city, postal_code: postal_code)
  	ActiveShipping::Location.new(country: "US", state: "CA", city: "Beverly Hills", postal_code: "90210")
  end

  def packages
    package = ActiveShipping::Package.new(25, [24, 24, 9], units: :imperial)
  end

  def get_rates_from_shipper(shipper)
    response = shipper.find_rates(origin, destination, packages)
    response.rates.sort_by(&:price)
  end

  def ups_rates
    ups = ActiveShipping::UPS.new(login: 'your ups login', password: 'your ups password', key: 'your ups xml key')
    get_rates_from_shipper(ups)
  end

  def fedex_rates
    fedex = ActiveShipping::FedEx.new(login: "119096262", password: "BA92ZG7YpiXH7O2A0dIrJC9Yh", key: "q41znXQdFHQ2cVrJ", account: "510087500", :test => true)
    get_rates_from_shipper(fedex)
  end

  def usps_rates
    usps = ActiveShipping::USPS.new(login: 'your usps account number', password: 'your usps password')
    get_rates_from_shipper(usps)
  end

end
