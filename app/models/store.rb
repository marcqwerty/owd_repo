class Store < ActiveRecord::Base
	acts_as_mappable :default_units => :miles,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude

	# reverse_geocoded_by :latitude, :longitude do |obj,results|
	#   if geo = results.first
	#     obj.city    = geo.city
	#     obj.zipcode = geo.postal_code
	#     obj.country = geo.country_code
	#   end
	# end
	# after_validation :reverse_geocode
end
