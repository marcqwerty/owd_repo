class UserMailer < ApplicationMailer
	# default from: "olt_beta@gmail.com"

  def send_compare_price(email, fullname)
    @fullname = fullname
    @email    = email
    mail(to: @email, subject: 'TESTMAIL - Compare Price')
  end
end
