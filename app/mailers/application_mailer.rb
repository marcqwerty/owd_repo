class ApplicationMailer < ActionMailer::Base
  default from: "olt_beta@gmail.com"
  layout 'mailer'
end
