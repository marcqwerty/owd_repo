module ApplicationHelper
	def inventory_lookup(mspn)
		inventories = TireInventory.where(MSPN: mspn)
		return inventories.first
	end

	def look_for_wheels(sectionwidth, aspectratio, rimdiameter)
		# tire_size = TireSize.select("CARTIREID").where({SIZE_PART_1: Sectionwidth, SIZE_PART_2: Aspectratio, SIZE_PART_3: Rimdiameter})
		# return tire_size.first
	end

	def look_up_brand(brand)
		if brand == 'ACCELERA'
			return 'l1.jpg'
		elsif brand == 'ACHILLES'
			return 'l2.jpg'
		elsif brand == 'ARROYO'
			return 'arroyo.png'	
		elsif brand == 'ATTURO'
			return 'atturo.png'
		elsif brand == 'BFGOODRICH'
			return 'l4.jpg'
		elsif brand == 'BRIDGESTONE'
			return 'l5.jpg'
		elsif brand == 'COKERTIRE'
			return 'l6.jpg'
		elsif brand == 'CONTINENTAL'
			return 'l7.jpg'
		elsif brand == 'COOPER'
			return 'l8.jpg'	
		elsif brand == 'DELINTE'
			return 'l9.jpg'
		elsif brand == 'DICKCEPEK'
			return 'l10.jpg'
		elsif brand == 'DUNLOP'
			return 'l11.jpg'
		elsif brand == 'FALKEN'
			return 'l12.jpg'	
		elsif brand == 'FIRESTONE'
			return 'l13.jpg'
		elsif brand == 'FUEL'
			return 'l14.jpg'
		elsif brand == 'FUZION'
			return 'l15.jpg'
		elsif brand == 'GENERAL'
			return 'l16.jpg'
		elsif brand == 'GOODYEAR'
			return 'l17.jpg'
		elsif brand == 'HANKOOK'
			return 'l18.jpg'
		elsif brand == 'KELLY'
			return 'l19.jpg'
		elsif brand == 'KUMHO'
			return 'l20.jpg'
		elsif brand == 'LEXANI'
			return 'l21.jpg'
		elsif brand == 'LIONHART'
			return 'l22.jpg'
		elsif brand == 'LIZETTI'
			return 'l23.jpg'
		elsif brand == 'MASTERCRAFT'
			return 'l24.jpg'
		elsif brand == 'MICHELIN'
			return 'l25.jpg'
		elsif brand == 'MICKEYTHOMPSON'
			return 'l26.jpg'
		elsif brand == 'NEXEN'
			return 'l27.jpg'
		elsif brand == 'NITTO'
			return 'l28.jpg'
		elsif brand == 'PIRELLI'
			return 'l29.jpg'
		elsif brand == 'RBP'
			return 'l30.jpg'
		elsif brand == 'SUMITOMO'
			return 'l31.jpg'
		elsif brand == 'TOYO'
			return 'l32.jpg'
		elsif brand == 'UNIROYAL'
			return 'l33.jpg'
		elsif brand == 'YOKOHAMA'
			return 'l34.jpg'
		end
	end

	def look_up_wheel_brand(brand)
		if brand == 'Katana'
			return 'w1.jpg'
		elsif brand == 'Dolce'
			return 'w2.jpg'
		elsif brand == '2Crave'
			return 'w3.jpg'
		elsif brand == 'Avat'
			return 'w4.jpg'
		elsif brand == 'Avenue'
			return 'w5.jpg'
		elsif brand == 'Beyern'
			return 'w6.jpg'
		elsif brand == 'Black Rhino'
			return 'w7.jpg'
		elsif brand == 'Coventry'
			return 'w8.jpg'
		elsif brand == 'Cray'
			return 'w9.jpg'
		elsif brand == 'Dub'
			return 'w10.jpg'
		elsif brand == 'Foose'
			return 'w11.jpg'
		elsif brand == 'Fuel'
			return 'w12.jpg'
		elsif brand == 'Hostile'
			return 'w13.jpg'
		elsif brand == 'KX-Offroad'
			return 'w14.jpg'
		elsif brand == 'Lumaria'
			return 'w15.jpg'
		elsif brand == 'Mach'
			return 'w16.jpg'
		elsif brand == 'Mandrus'
			return 'w17.jpg'
		elsif brand == 'MKW'
			return 'w18.jpg'
		elsif brand == 'Niche'
			return 'w19.jpg'
		elsif brand == 'Ninja'
			return 'w20.jpg'
		elsif brand == 'Redbourne'
			return 'w21.jpg'
		elsif brand == 'TSW'
			return 'w22.jpg'
		elsif brand == 'US'
			return 'w23.jpg'
		# elsif brand == 'Victor Equipment'
		# 	return 'w24.jpg'
		elsif brand == '2Crave'
			return 'w25.jpg'
		elsif brand == 'XPWheels'
			return 'w26.jpg'
		elsif brand == 'Forgiato'
			return 'w27.jpg'
		elsif brand == 'LEXANI'
			return 'w28.jpg'
		elsif brand == 'Rohana'
			return 'w29.jpg'
		elsif brand == 'Pacer'
			return 'pacer.jpg'
		elsif brand == 'Raceline'
			return 'raceline.png'
		elsif brand == 'Vision'
			return 'vision.png'
		elsif brand == 'Hurst'
			return 'hurst.png'
		elsif brand == 'Cragar'
			return 'cragar.png'
		elsif brand == 'Primax'
			return 'primax.gif'
		elsif brand == 'Ultra'
			return 'ultra.gif'
		elsif brand == 'Walker Evans Racing'
			return 'walker_evan_racing.gif'
		elsif brand == 'American Racing'
			return 'american_racing.png'
		elsif brand == 'Motegi'
			return 'motegi.png'
		elsif brand == 'XXR'
			return 'xxr.gif'
		elsif brand == 'Fifteen 52'
			return 'fifteen52.jpg'
		elsif brand == 'Victor Equipment'
			return 'victor_equipment.gif'
		elsif brand == 'Asanti'
			return 'asanti.gif'		
		end
	end

	def price_lookup(brand)
		if brand == '2Crave'
			@price = 70.2
		elsif brand == 'Advanti Racing'
			@price = 71
		elsif brand == 'Adventus'
			@price = 72
		elsif brand == 'Asanti'
			@price = 73
		elsif brand == 'ATX Series'
			@price = 70.3
		elsif brand == 'Avat'
			@price = 69
		elsif brand == 'Beyern'
			@price = 80.5
		elsif brand == 'Big Bang'
			@price = 81
		elsif brand == 'Black Rhino'
			@price = 69
		elsif brand == 'BMF Wheels'
			@price = 73.4
		elsif brand == 'Bravado'
			@price = 75
		elsif brand == 'Centerline'
			@price = 77
		elsif brand == 'Concept One'
			@price = 78.9
		elsif brand == 'Coventry'
			@price = 72.9
		elsif brand == 'Cray'
			@price = 72.2
		elsif brand == 'Cruiser Alloy'
			@price = 70.9
		elsif brand == 'Deegan 38'
			@price = 77.1
		elsif brand == 'Diamo'
			@price = 69.9
		elsif brand == 'Dick Cepek'
			@price = 70.2
		elsif brand == 'Dolce'
			@price = 77
		elsif brand == 'Drifz'
			@price = 80
		elsif brand == 'Dropstars'
			@price = 81
		elsif brand == 'Pacer'
			@price = 72
		elsif brand == 'Raceline'
			@price = 73
		elsif brand == 'Vision'
			@price = 73.5
		elsif brand == 'Hurst'
			@price = 72.3
		elsif brand == 'Cragar'
			@price = 77.2
		elsif brand == 'Primax'
			@price = 71.8
		elsif brand == 'Ultra'
			@price = 72.3
		elsif brand == 'Walker Evans Racing'
			@price = 74.5
		elsif brand == 'American Racing'
			@price = 75
		elsif brand == 'Motegi'
			@price = 76.1
		elsif brand == 'XXR'
			@price = 72.25
		elsif brand == 'Fifteen 52'
			@price = 79.5
		elsif brand == 'Victor Equipment'
			@price = 70.25
		elsif brand == 'Asanti'
			@price = 68.5
		else
			@price = 75.1
		end               
	end

	def compute_ratings(brand, model)
		brand_id = Brand.find_by_brand_name brand
		@id = BrandModel.where(brand_id: brand_id.id, brand_model_name: model)
		
		
		if @id.empty?
			@stars = 0
		else
			reviews = Review.select("stars").where(brand_id: brand_id.id, brand_model_id: @id.first.id).pluck(:stars)	
			@count = reviews.count
			if @count > 0
				@average = reviews.compact.inject(:+)
				if !@average.nil?
					@stars = @average / @count
				else
					@stars = 0
				end
			else
				@stars = 0
			end
		end
		if @stars == 0
			@stars = 0
		elsif @stars >= 1 && @stars < 2 
			@stars = 1
		elsif @stars >= 2 && @stars < 3
			@stars = 2
		elsif @stars >= 3 && @stars < 4
			@stars = 3
		elsif @stars >= 4 && @stars < 5
			@stars = 4
		elsif @stars >= 5 && @stars < 5
			@stars = 5
		end
	end

	def brand_details(brand, brand_model_name)
		brand_id = Brand.select(:id).find_by_brand_name brand
		@data = BrandModel.where({brand_id: brand_id, brand_model_name: brand_model_name})
		@data.first
	end

	def reviews(brand, model)
		brand_id = Brand.select(:id).find_by_brand_name brand
		@id = BrandModel.where(brand_id: brand_id, brand_model_name: model)
		@data = Review.where(brand_id: brand_id, brand_model_id: @id.first.id)
	end

	def count_reviews(brand, model)
		brand_id = Brand.select(:id).find_by_brand_name brand
		@id = BrandModel.where(brand_id: brand_id, brand_model_name: model)
		if @id.empty?
			@val = 0
		else
			@val = Review.where(brand_id: brand_id, brand_model_id: @id.first.id)
			@val.count
		end
	end

	def wheels_lookup(wheel_diameter, wheel_width)
		@wheels_list = WheelInventory.where({WheelDiameter: params[:sp3], WheelWidth: params[:sp1]})
	end

	def tire_ratings(brand, model, cond)
		brand_id = Brand.select(:id).find_by_brand_name brand
		@id = BrandModel.where(brand_id: brand_id, brand_model_name: model)
		if cond == "dry"
			@data = Review.select("dry").where(brand_id: brand_id, brand_model_id: @id.first.id).pluck(:dry)
		elsif cond == "wet"
			@data = Review.select("wet").where(brand_id: brand_id, brand_model_id: @id.first.id).pluck(:wet)
		elsif cond == "snow"
			@data = Review.select("snow").where(brand_id: brand_id, brand_model_id: @id.first.id).pluck(:snow)
		elsif cond == "comfort"
			@data = Review.select("comfort").where(brand_id: brand_id, brand_model_id: @id.first.id).pluck(:comfort)
		elsif cond == "treadwear"
			@data = Review.select("treadwear").where(brand_id: brand_id, brand_model_id: @id.first.id).pluck(:treadwear)
		elsif cond == "noise"
			@data = Review.select("noise").where(brand_id: brand_id, brand_model_id: @id.first.id).pluck(:noise)
		end	
		@y = @data.count * 5
		@x = @data.inject(:+).to_f
		@z = (@x / @y) * 100
		return @z.to_i / 20

		# if @z == 0
		# 	@zz = 0
		# elsif @z >= 1.0 && @z < 20.0 
		# 	@zz = 1
		# elsif @z >= 21.0 && @z < 40.0
		# 	@zz = 2
		# elsif @z >= 41.0 && @z < 60.0
		# 	@zz = 3
		# elsif @z >= 61.0 && @z < 80.0
		# 	@zz = 4
		# elsif @z >= 81.0 && @z < 100.0
		# 	@zz = 5
		# end

		# return @zz
		
	end
end
