Rails.application.routes.draw do

  devise_for :users
  
  resources :inventories
  resources :reviews
  root 'mainpages#index'

  namespace :admin do
    resources :brands
    resources :brand_models
    resources :cms_categories
    resources :reviews
  end



  resources :carts
  resources :cart_checkouts
  resources :wheels
  resources :tires do 
    get :tire_brands, on: :collection
  end

  resources :users

  post "/hook" => "cart_checkouts#hook"
  post "/cart_checkouts/:id" => "cart_checkouts#show"

 
  get 'mainpages/update_years', :as => 'update_years'
  get 'mainpages/update_makes', :as => 'update_makes'
  get 'mainpages/update_models', :as => 'update_models'
  get 'mainpages/update_options', :as => 'update_options'

  get 'mainpages/update_aspectratio', :as => 'update_aspectratio'
  get 'mainpages/update_rimdiameter', :as => 'update_rimdiameter'
  get 'mainpages/update_wheelwidth', :as => 'update_wheelwidth'

  get 'mainpages/search_tires', :as => 'search_tires'
  get 'mainpages/search_wheels', :as => 'search_wheels'
  get 'mainpages/search_wheels_tires', :as => 'search_wheels_tires' 
  get 'mainpages/search_entry', :as => 'search_entry'
  get 'mainpages/list_all_tires', :as => 'list_all_tires'
  get 'mainpages/list_all_wheels', :as => 'list_all_wheels'
  get 'mainpages/search_locations', :as => 'search_locations'
  get 'mainpages/product_page', :as => 'product_page'
  get 'mainpages/search_by_size', :as => 'search_by_size'
  get 'mainpages/list_wheels_tires', :as => 'list_wheels_tires'
  get 'mainpages/compare_price', :as => 'compare_price'
  get 'mainpages/comparison', :as => 'comparison'

  # for tires and wheels per brand search
  get 'mainpages/tires_per_brand_list', :as => 'tires_per_brand_list'
  

  # get 'mainpages/cart', :as => 'cart'
  get '/brands', :to => redirect('/brands.html')

  # for user signup
  # get 'users/new', :as => 'signup'

  # error pages
  %w( 404 422 500 503 ).each do |code|
    get code, :to => "errors#show", :code => code
  end
end
